const knex = require("../dbConfig");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = {
  async index(req, res) {
    const { email, senha } = req.body;
    if (!email || !senha) {
      res.status(200).json({ erro: "true", msg: "Enviar e-mail e senha"});
      return;
    }
    try {
      const dados = await knex("usuarios").where({ email });
      if (dados.length == 0) {
        res.status(200).json({ erro: "true", msg: "Email não cadastrado"});
        return;
      }
      if (bcrypt.compareSync(senha, dados[0].senha)) {
        const token = jwt.sign(
          {
            usuario_id: dados[0].id,
            usuario_nome: dados[0].nome,
          },
          process.env.JWT_KEY,
          { expiresIn: "10h" }
        );
        var adm = dados[0].adm == 0 ? false : true;
        res.status(200).json({ token, 
          user: {
            nome: dados[0].nome, 
            foto: dados[0].foto,
            id: dados[0].idU,
            adm: adm
        } 
        });
      } else {
        res.status(200).json({ erro: "true", msg: "Senha incorreta" });
      }
    } catch (error) {
      res.status(200).json({ erro: "true", msg: error.message});
    }
  }
};
