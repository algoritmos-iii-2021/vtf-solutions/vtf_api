const express = require("express");
const cors = require('cors')
const routes = express.Router();

routes.use(cors())

const userController = require("../controllers/user");
const loginTest = require("../middlewares/login");
const login = require("../controllers/login");
const quest = require("../controllers/questao")
const resp = require("../controllers/respostas")

routes
  .get("/users", loginTest, userController.users) // Retorna todos os usuarios cadastrados
  .post("/users", userController.createUser) // Cadastra um usuario
  .delete("/user/:id", loginTest, userController.deleteUser) // Remove um usuario a partir do seu ID
  .put("/user/:id", loginTest, userController.updateUser); // Altera a senha de um usuario a partir do seu ID, necessita que passe nova senha por json

routes
  .post("/login", login.index) // Login 
  .put("/userAdm/:idU", userController.updateAdm); // Torna o usuario um administrador
  
routes
  .get("/quest", loginTest, quest.questao) // Retorna todas questoes
  .get('/questResp', loginTest, quest.questaoRespondida) // Retornar todas questoes e suas respostas
  .get("/quest/:id", loginTest, quest.questaoUser) // Retorna questoes vinculadas a um id de usuario
  .post("/quest/:id", loginTest, quest.addQuestion) // adiciona uma questão vinculada a um id de usuario
  .delete("/quest/:id", loginTest, quest.delQuestion) // Remove uma questão por seu ID
  .put("/quest/:id", loginTest, quest.updateQuestion); // Altera a pergunta por seu ID

routes
  .get("/resp", loginTest, resp.respostas) // Retorna todas respostas
  .get("/resp/:id", loginTest, resp.respUsers) // Retornar respostas vinculadas a um id de usuario
  .post("/resp/:idQ/:idU", loginTest, resp.addRespostas) // Adicona respostas com o ID da questao a ser respondida e o ID do usuario que respondeu
module.exports = routes;
