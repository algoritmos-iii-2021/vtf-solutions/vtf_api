const knex = require("../dbConfig");

module.exports = {
  async respostas(req, res) {
    try {
      const dados = await knex("respostas")
        .select(
          "r.idR",
          "r.idQuestao",
          "r.resposta",
          "r.created_at",
          "u.nome as nome",
          "u.foto as foto"
        )
        .from("respostas as r")
        .leftJoin("usuarios as u", "r.idUser", "U.idU");
      res.status(200).json({ erro: "false", dados: dados });
    } catch (error) {
      res.status(200).json({ erro: "true", msg: error.message });
    }
  },
  async respUsers(req, res) {
    const idUser = req.params.id;
    try {
      const dados = await knex("respostas").where({ idUser });
      res.status(200).json({ erro: "false", dados: dados });
    } catch (error) {
      res.status(200).json({ erro: "true", msg: error.message });
    }
  },
   async addRespostas(req, res) {
    const idQuestao = req.params.idQ;
    const idUser = req.params.idU;
    const { resposta } = req.body;

    if (!resposta) {
      res.status(200).json({ erro: "true", msg: "Enviar Resposta" });
      return;
    }
    try {
      await knex("respostas").insert({
        idUser,
        idQuestao,
        resposta,
      });
      res.status(200).json({ erro: "false", msg: "Resposta Salva" });
    } catch (error) {
      res.status(200).json({ erro: "true", msg: error.message });
    }
  },
};
