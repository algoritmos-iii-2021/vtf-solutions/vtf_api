const knex = require("../dbConfig");

module.exports = {
  async questao(req, res) {
    try {
      const dados = await knex("questoes", "respostas")
        .select(
          "q.idQ",
          "q.questao",
          "q.created_at",
          "u.nome as usuario",
          "u.foto"
        )
        .from("questoes as q")
        .leftJoin("usuarios as u", "q.idUser", "u.idU");
      res.status(200).json({ erro: "false", dados: dados });
    } catch (error) {
      res.status(200).json({ erro: "true", msg: error.message });
    }
  },
  async questaoRespondida(req, res) {
    try {
      const dados = [];
      const questoes = await knex("questoes")
        .select(
          "q.idQ",
          "q.questao",
          "q.created_at",
          "u.nome as usuario",
          "u.foto"
        )
        .from("questoes as q")
        .orderBy('idQ', "desc")
        .leftJoin("usuarios as u", "q.idUser", "u.idU");

      for (var i = 0; i < questoes.length; i++) {
        const respostas = await knex("respostas")
          .where({ idQuestao: questoes[i].idQ })
          .select(
            "r.idR",
            "r.idQuestao",
            "r.resposta",
            "r.created_at",
            "u.nome as nome",
            "u.foto as foto"
          )
          .from("respostas as r")
          .leftJoin("usuarios as u", "r.idUser", "U.idU");

        if (!respostas.length) {
          dados.push({
            idq: questoes[i].idQ,
            questao: questoes[i].questao,
            criado: questoes[i].created_at,
            usuario: questoes[i].usuario,
            foto: questoes[i].foto
          });
        } else {
          dados.push({
            idq: questoes[i].idQ,
            questao: questoes[i].questao,
            criado: questoes[i].created_at,
            usuario: questoes[i].usuario,
            foto: questoes[i].foto,
            respostas: respostas,
          });
        }
      }
      res.status(200).json({ erro: "false", dados: dados });
    } catch (error) {
      res.status(200).json({ erro: "true", msg: error.message });
    }
  },

  async questaoUser(req, res) {
    try {
      const idUser = req.params.id;
      const dados = await knex("questoes")
        .select(
          "q.idQ",
          "q.questao",
          "q.created_at",
          "u.nome as usuario",
          "u.foto"
        )
        .from("questoes as q")
        .leftJoin("usuarios as u", "q.idUser", "u.idU")
        .where({ idUser });
      res.status(200).json({ erro: "false", dados: dados });
    } catch (error) {
      res.status(200).json({ erro: "true", msg: error.message });
    }
  },

  async addQuestion(req, res) {
    const idUser = req.params.id;
    const { questao } = req.body;
    if (!questao) {
      res.status(200).json({ erro: "true", msg: "Enviar questao." });
      return;
    }
    try {
      const newQuest = await knex("questoes").insert({
        idUser,
        questao,
      });
      res.status(200).json({ erro: "false", id: newQuest[0] });
    } catch (error) {
      res.status(200).json({ erro: "true", msg: error.message });
    }
  },

  async delQuestion(req, res) {
    const idQ = req.params.id;
    try {
      await knex("questoes").del().where({ idQ });
      res.status(200).json({ erro: "false", msg: "Questão Deletada" });
    } catch (error) {
      res.status(200).json({ erro: "true", msg: error.message });
    }
  },

  async updateQuestion(req, res) {
    const idQ = req.params.id;
    const { questao } = req.body;
    if (!questao) {
      res.status(200).json({ erro: "true", msg: "Enviar questao." });
      return;
    }
    try {
      await knex("questoes").update("questao", questao).where({ idQ });
      res.status(200).json({ erro: "false", msg: "Questão Alterada" });
    } catch (error) {
      res.status(200).json({ erro: "true", msg: error.message });
    }
  },
};
